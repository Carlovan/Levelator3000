use super::Item;
use std::collections::HashMap;

pub type Inventory = HashMap<Item, u32>;

trait Mergeable {
    fn merge(&self, other: &Self) -> Self;
}

impl Mergeable for Inventory {
    fn merge(&self, other: &Self) -> Self {
        let mut new_name = self.clone();
        for (item, amount) in other {
            *new_name.entry(item.clone()).or_insert(0) += amount;
        }
        new_name
    }
}

