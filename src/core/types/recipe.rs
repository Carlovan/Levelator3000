use super::Item;
use super::Inventory;
use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct Recipe {
    pub name: String,
    pub exp: u32,
    pub ingredients: Inventory,
    pub result: Item,
}

