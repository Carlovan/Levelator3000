// use std::hash::{Hash, Hasher};
// use serde::Deserialize;

pub type Item = String;

// #[derive(Debug, Clone, Deserialize)]
// pub struct Item {
//     pub name: String,
// }

// impl PartialEq for Item {
//     fn eq(&self, other: &Item) -> bool {
//         self.name == other.name
//     }
// }

// impl Eq for Item {}

// impl Hash for Item {
//     fn hash<H: Hasher>(&self, state: &mut H) {
//         self.name.hash(state)
//     }
// }

// impl From<&str> for Item {
//     fn from(name: &str) -> Self {
//         Item {
//             name: String::from(name)
//         }
//     }
// }
