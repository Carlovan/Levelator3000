use crate::core::types::Inventory;
use std::fs::{File, read_to_string};
use serde::{Deserialize, de};
use regex::Regex;

use crate::core::types::Item;
use crate::core::types::Recipe;

pub const CORGY_FILE: &str = "ricette.json";

fn deserialize_items<'de, D>(deserializer: D) -> Result<Vec<Item>, D::Error>
where D: de::Deserializer<'de> {
    let items: Vec<CorgyItem> = de::Deserialize::deserialize(deserializer)?;
    Ok(items.into_iter().map(|item| item.name).collect())
}

#[derive(Deserialize)]
struct CorgyItem { name: Item }

#[derive(Deserialize)]
struct CorgyInput {
    ricette: Vec<Recipe>,
    #[serde(deserialize_with = "deserialize_items")]
    items: Vec<Item>,
}

#[derive(Deserialize)]
struct Adventure { adventure: CorgyInput }

pub fn load_corgy_json(file_path: &str) -> (Vec<Recipe>, Vec<Item>){
    let parsed: Adventure = serde_json::from_reader(
        File::open(file_path)
        .expect("Cannot open file")
    ).expect("Cannot deserialize");
    (parsed.adventure.ricette, parsed.adventure.items)
}

pub fn load_zaino_output(file_path: &str) -> Inventory {
    let item_regex = Regex::new(r"(?m)^ ?- (.+) \[[A-Z]+\] (?:\[usabile\] )?x(\d+)$").unwrap();
    let inventory = read_to_string(file_path).unwrap();
    return item_regex.captures_iter(&inventory)
        .map(|cap| {
            (
                String::from(cap.get(1).unwrap().as_str()),
                cap.get(2).unwrap().as_str().parse::<u32>().unwrap()
            )
        })
        .collect();
}
