use levelator3000::core::types::Item;
use std::collections::HashMap;
use levelator3000::core::loaders::*;

fn print_type<T>(_: &T) {
    println!("{}", std::any::type_name::<T>());
}

fn main() {
    let (recipes, items) = load_corgy_json("ricette_test.json");
    let item_recipe = recipes.iter().map(|r| (r.result.clone(), r)).collect::<HashMap<_, _>>();
    println!("{:#?}", recipes);
    println!("{:#?}", item_recipe);
    print_type(&item_recipe);
    println!("{:?}", items);

    let zaino = load_zaino_output("zaino.txt");
    println!("{:?}", zaino);

    let missing_items = zaino.keys().filter(|item| !items.contains(item)).cloned().collect::<Vec<Item>>();
    if missing_items.is_empty() {
        println!("All items are in corgy");
    } else {
        println!("Item(s) not in corgy!\n{}", missing_items.iter().map(|x| "   ".to_string() + x).collect::<Vec<String>>().join("\n"));
    }
}
